# IOC for LEBT vacuum gauge controllers and gauges

## Used modules

*   [vac_ctrl_mks946_937b](https://gitlab.esss.lu.se/e3/wrappers/vac/e3-vac_ctrl_mks946_937b)


## Controlled devices

*   LEBT-010:Vac-VEG-00011
    *   LEBT-010:Vac-VGP-00021
    *   LEBT-010:Vac-VGP-00081
*   LEBT-010:Vac-VEG-10001
    *   LEBT-010:Vac-VGP-10000
    *   LEBT-010:Vac-VGP-30000
    *   LEBT-010:Vac-VGC-10000
    *   LEBT-010:Vac-VGC-30000
*   LEBT-010:Vac-VEVMC-10001
    *   LEBT-010:Vac-VVMC-01100
    *   LEBT-010:Vac-VGD-10000
