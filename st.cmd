#
# Module: essioc
#
require essioc

#
# Module: vac_ctrl_mks946_937b
#
require vac_ctrl_mks946_937b,4.5.2


#
# Setting STREAM_PROTOCOL_PATH
#
epicsEnvSet(STREAM_PROTOCOL_PATH, "${vac_ctrl_mks946_937b_DB}")


#
# Module: essioc
#
iocshLoad("${essioc_DIR}/common_config.iocsh")

#
# Device: LEBT-010:Vac-VEG-00011
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEG-00011, BOARD_A_SERIAL_NUMBER = 1509231040, BOARD_B_SERIAL_NUMBER = 1609141318, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4001")

#
# Device: LEBT-010:Vac-VGP-00021
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-00021, CHANNEL = A1, CONTROLLERNAME = LEBT-010:Vac-VEG-00011")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-00021, RELAY = 1, RELAY_DESC = 'Process PLC: Manifold under vacuum'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-00021, RELAY = 2, RELAY_DESC = 'Process PLC: (LEBT-010:VAC-VVA-02100 + LEBT-010:VAC-VVA-03100) Local protection'")

#
# Device: LEBT-010:Vac-VGP-00081
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-00081, CHANNEL = A2, CONTROLLERNAME = LEBT-010:Vac-VEG-00011")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-00081, RELAY = 1, RELAY_DESC = 'Process PLC: Manifold under vacuum'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-00081, RELAY = 2, RELAY_DESC = 'Process PLC: (LEBT-010:VAC-VVA-06100 + LEBT-010:VAC-VVA-07100) Local protection'")

#
# Device: LEBT-010:Vac-VEG-10001
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEG-10001, BOARD_A_SERIAL_NUMBER = 1509231115, BOARD_B_SERIAL_NUMBER = 1609141305, BOARD_C_SERIAL_NUMBER = 1412221837, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4002")

#
# Device: LEBT-010:Vac-VGP-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-10000, CHANNEL = A1, CONTROLLERNAME = LEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-10000, RELAY = 1, RELAY_DESC = 'Process PLC: Atmospheric pressure'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-10000, RELAY = 2, RELAY_DESC = 'Process PLC: Vacuum -> Threshold to start the VPTs'")

#
# Device: LEBT-010:Vac-VGP-30000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgp.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-30000, CHANNEL = A2, CONTROLLERNAME = LEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-30000, RELAY = 1, RELAY_DESC = 'Process PLC: Atmospheric pressure'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGP-30000, RELAY = 2, RELAY_DESC = 'Process PLC: Vacuum -> Threshold to start the VPTs'")

#
# Device: LEBT-010:Vac-VGC-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-10000, CHANNEL = B1, CONTROLLERNAME = LEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-10000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-10000, RELAY = 2, RELAY_DESC = 'Process PLC: High vacuum threshold; starting finished. (transition from starting to started)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-10000, RELAY = 3, RELAY_DESC = 'Process PLC: Gas Injection VVA Interlock'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-10000, RELAY = 4, RELAY_DESC = 'VEGA-10001 Pressure interlock'")

#
# Device: LEBT-010:Vac-VGC-30000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-30000, CHANNEL = C1, CONTROLLERNAME = LEBT-010:Vac-VEG-10001")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-30000, RELAY = 1, RELAY_DESC = 'Interlock PLC: Gates Valves Interlock (MPS)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-30000, RELAY = 2, RELAY_DESC = 'Process PLC: High vacuum threshold; starting finished. (transition from starting to started)'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-30000, RELAY = 3, RELAY_DESC = 'Process PLC: Not used'")
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_relay_desc.iocsh", "DEVICENAME = LEBT-010:Vac-VGC-30000, RELAY = 4, RELAY_DESC = 'not wired'")

#
# Device: LEBT-010:Vac-VEVMC-10001
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_ctrl_mks946_937b_moxa.iocsh", "DEVICENAME = LEBT-010:Vac-VEVMC-10001, BOARD_A_SERIAL_NUMBER = 1601141134, BOARD_B_SERIAL_NUMBER = 1511060832, IPADDR = lebt-vac-sec-10001.tn.esss.lu.se, PORT = 4003")

#
# Device: LEBT-010:Vac-VVMC-01100
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_mfc_mks_gv50a.iocsh", "DEVICENAME = LEBT-010:Vac-VVMC-01100, CHANNEL = A1, CONTROLLERNAME = LEBT-010:Vac-VEVMC-10001")

#
# Device: LEBT-010:Vac-VGD-10000
# Module: vac_ctrl_mks946_937b
#
iocshLoad("${vac_ctrl_mks946_937b_DIR}/vac_gauge_mks_vgd.iocsh", "DEVICENAME = LEBT-010:Vac-VGD-10000, CHANNEL = B1, CONTROLLERNAME = LEBT-010:Vac-VEVMC-10001")

#
# IOC: VacS-LEBT:SC-IOC-110
# Load a possible IOC-custom .iocsh file
#
iocshLoad("$(E3_CMD_TOP)/iocsh/lebt.iocsh")
